import 'package:flutter/material.dart';

void main() {
  runApp(ProsApp());
}

final mainImage1 = Image.asset(
  'images/img1.png',
  height: 100,
  width: 100,
  fit: BoxFit.cover,
);
final mainImage2 = Image.asset(
  'images/img2.png',
  height: 100,
  width: 100,
  fit: BoxFit.cover,
);
final mainImage3 = Image.asset(
  'images/img3.png',
  height: 100,
  width: 100,
  fit: BoxFit.cover,
);
final mainImage4 = Image.asset(
  'images/img4.png',
  height: 100,
  width: 100,
  fit: BoxFit.cover,
);
final mainImage5 = Image.asset(
  'images/img5.png',
  height: 100,
  width: 100,
  fit: BoxFit.cover,
);
final mainImage6 = Image.asset(
  'images/img6.png',
  height: 100,
  width: 100,
  fit: BoxFit.cover,
);

class ProsApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var container = Container(
      height: 60,
      width: MediaQuery.of(context).size.width / 3,
      decoration: BoxDecoration(
        color: Colors.green[600],
      ),
      child: Icon(Icons.home),
    );

    var container2 = Container(
      height: 60,
      width: MediaQuery.of(context).size.width / 3,
      decoration: BoxDecoration(
        color: Colors.green[600],
      ),
      child: Icon(Icons.fitness_center),
    );

    var container3 = Container(
        height: 60,
        width: MediaQuery.of(context).size.width / 3,
        decoration: BoxDecoration(
          color: Colors.green[600],
        ),
        child: Icon(Icons.settings));

    return Scaffold(
      bottomNavigationBar: Row(
        children: <Widget>[container, container2, container3],
      ),
      //debugShowCheckedModeBanner: false,
      appBar: AppBar(
        title: Text('MENU', textAlign: TextAlign.center),
        backgroundColor: Colors.green,
      ),
      body: Center(
          child: Column(children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
          child: Table(
            //  border: TableBorder.all(),
            children: [
              TableRow(children: [
                Column(children: [
                  mainImage1,
                  FlatButton(
                    child: Text('อาหารจานเดียว'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          return DetailScreen1();
                        }),
                      );
                    },
                  ),
                ]),
                Column(children: [
                  mainImage2,
                  FlatButton(
                    child: Text('ต้ม'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          return DetailScreen2();
                        }),
                      );
                    },
                  ),
                ]),
              ]),
              TableRow(children: [
                Column(children: [
                  mainImage3,
                  FlatButton(
                    child: Text('ผัด'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          return DetailScreen3();
                        }),
                      );
                    },
                  ),
                ]),
                Column(children: [
                  mainImage4,
                  FlatButton(
                    child: Text('อบ'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          return DetailScreen4();
                        }),
                      );
                    },
                  ),
                ]),
              ]),
              TableRow(children: [
                Column(children: [
                  mainImage5,
                  FlatButton(
                    child: Text('ขนมหวาน'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          return DetailScreen5();
                        }),
                      );
                    },
                  ),
                ]),
                Column(children: [
                  mainImage6,
                  FlatButton(
                    child: Text('เครื่องดื่ม'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          return DetailScreen6();
                        }),
                      );
                    },
                  ),
                ]),
              ]),
            ],
          ),
        ),
      ])),
    );
  }
}

class DetailScreen1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var container = Container(
      height: 60,
      width: MediaQuery.of(context).size.width / 3,
      decoration: BoxDecoration(
        color: Colors.green[600],
      ),
      child: Icon(Icons.home),
    );

    var container2 = Container(
      height: 60,
      width: MediaQuery.of(context).size.width / 3,
      decoration: BoxDecoration(
        color: Colors.green[600],
      ),
      child: Icon(Icons.fitness_center),
    );

    var container3 = Container(
        height: 60,
        width: MediaQuery.of(context).size.width / 3,
        decoration: BoxDecoration(
          color: Colors.green[600],
        ),
        child: Icon(Icons.settings));
    return Scaffold(
      bottomNavigationBar: Row(
        children: <Widget>[container, container2, container3],
      ),
      appBar: AppBar(
        title: Text(
          'อาหารจานเดียว',
          //textAlign: TextAlign.center,
        ),
        backgroundColor: Colors.green,
      ),
      body: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ข้าวคะน้าหมูกรอบ 1 จาน ให้พลังงาน 670 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ข้าวกะเพราไก่ ไข่ดาว ให้พลังงาน 630 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ข้าวมันไก่ ให้พลังงาน 596 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ข้าวขาหมู ให้พลังงาน 690 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ข้าวไข่เจียว ให้พลังงาน 445 Kcal'),
              ])),
        ],
      ),
    );
  }
}

class DetailScreen2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var container = Container(
      height: 60,
      width: MediaQuery.of(context).size.width / 3,
      decoration: BoxDecoration(
        color: Colors.green[600],
      ),
      child: Icon(Icons.home),
    );

    var container2 = Container(
      height: 60,
      width: MediaQuery.of(context).size.width / 3,
      decoration: BoxDecoration(
        color: Colors.green[600],
      ),
      child: Icon(Icons.fitness_center),
    );

    var container3 = Container(
        height: 60,
        width: MediaQuery.of(context).size.width / 3,
        decoration: BoxDecoration(
          color: Colors.green[600],
        ),
        child: Icon(Icons.settings));
    return Scaffold(
      bottomNavigationBar: Row(
        children: <Widget>[container, container2, container3],
      ),
      appBar: AppBar(
        title: Text(
          'ต้ม',
          textAlign: TextAlign.center,
        ),
        backgroundColor: Colors.green,
      ),
      body: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ต้มข่าไก่ 1 ถ้วย ให้พลังงาน 210 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ต้มจับฉ่าย 1 ถ้วย ให้พลังงาน 180 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ต้มยำกุ้ง 1 ถ้วย ให้พลังงาน 65 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ต้มเลือดหมู ให้พลังงาน 120 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('เป็ดพะโล้ 1 ถ้วย ให้พลังงาน 110 Kcal'),
              ])),
        ],
      ),
    );
  }
}

class DetailScreen3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var container = Container(
      height: 60,
      width: MediaQuery.of(context).size.width / 3,
      decoration: BoxDecoration(
        color: Colors.green[600],
      ),
      child: Icon(Icons.home),
    );

    var container2 = Container(
      height: 60,
      width: MediaQuery.of(context).size.width / 3,
      decoration: BoxDecoration(
        color: Colors.green[600],
      ),
      child: Icon(Icons.fitness_center),
    );

    var container3 = Container(
        height: 60,
        width: MediaQuery.of(context).size.width / 3,
        decoration: BoxDecoration(
          color: Colors.green[600],
        ),
        child: Icon(Icons.settings));
    return Scaffold(
      bottomNavigationBar: Row(
        children: <Widget>[container, container2, container3],
      ),
      appBar: AppBar(
        title: Text(
          'ผัด',
          textAlign: TextAlign.center,
        ),
        backgroundColor: Colors.green,
      ),
      body: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ผัดผักบุ้งไฟแดง 1 จาน ให้พลังงาน 210 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ผัดวุ้นเส้นใส่ไข่ 1 จาน ให้พลังงาน 265 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ผัดเปรี้ยวหวานไก่ 1 จาน ให้พลังงาน 215 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('หมูผัดขิง 1 จาน ให้พลังงาน 270  Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ผัดไชโป๊วใส่ไข่ 1 จาน ให้พลังงาน 125 Kcal'),
              ])),
        ],
      ),
    );
  }
}

class DetailScreen4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var container = Container(
      height: 60,
      width: MediaQuery.of(context).size.width / 3,
      decoration: BoxDecoration(
        color: Colors.green[600],
      ),
      child: Icon(Icons.home),
    );

    var container2 = Container(
      height: 60,
      width: MediaQuery.of(context).size.width / 3,
      decoration: BoxDecoration(
        color: Colors.green[600],
      ),
      child: Icon(Icons.fitness_center),
    );

    var container3 = Container(
        height: 60,
        width: MediaQuery.of(context).size.width / 3,
        decoration: BoxDecoration(
          color: Colors.green[600],
        ),
        child: Icon(Icons.settings));
    return Scaffold(
      bottomNavigationBar: Row(
        children: <Widget>[container, container2, container3],
      ),
      appBar: AppBar(
        title: Text(
          'อบ',
          textAlign: TextAlign.center,
        ),
        backgroundColor: Colors.green,
      ),
      body: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('กุ้งอบวุ้นเส้น 1 จาน ให้พลังงาน 300 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ปลาช่อนอบเกลือ 1 ตัว ให้พลังงาน 220 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('หอยแมลงภู่อบหม้อดิน 4-5 ตัว ให้พลังงาน 85 Kcal'),
              ])),
        ],
      ),
    );
  }
}

class DetailScreen5 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var container = Container(
      height: 60,
      width: MediaQuery.of(context).size.width / 3,
      decoration: BoxDecoration(
        color: Colors.green[600],
      ),
      child: Icon(Icons.home),
    );

    var container2 = Container(
      height: 60,
      width: MediaQuery.of(context).size.width / 3,
      decoration: BoxDecoration(
        color: Colors.green[600],
      ),
      child: Icon(Icons.fitness_center),
    );

    var container3 = Container(
        height: 60,
        width: MediaQuery.of(context).size.width / 3,
        decoration: BoxDecoration(
          color: Colors.green[600],
        ),
        child: Icon(Icons.settings));
    return Scaffold(
      bottomNavigationBar: Row(
        children: <Widget>[container, container2, container3],
      ),
      appBar: AppBar(
        title: Text(
          'ขนมหวาน',
          textAlign: TextAlign.center,
        ),
        backgroundColor: Colors.green,
      ),
      body: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ข้าวเม่าทอด 2 ลูก ให้พลังงาน 418 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ข้าวเหนียวกะทิทุเรียน 1 ถ้วย ให้พลังงาน 225 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ข้าวหลาม 1 กระบอก ให้พลังงาน 230 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('บราวนี่ 1 ชิ้น ให้พลังงาน 340 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('วุ้นกะทิ 1 ชิ้น ให้พลังงาน 215 Kcal'),
              ])),
        ],
      ),
    );
  }
}

class DetailScreen6 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var container = Container(
      height: 60,
      width: MediaQuery.of(context).size.width / 3,
      decoration: BoxDecoration(
        color: Colors.green[600],
      ),
      child: Icon(Icons.home),
    );

    var container2 = Container(
      height: 60,
      width: MediaQuery.of(context).size.width / 3,
      decoration: BoxDecoration(
        color: Colors.green[600],
      ),
      child: Icon(Icons.fitness_center),
    );

    var container3 = Container(
        height: 60,
        width: MediaQuery.of(context).size.width / 3,
        decoration: BoxDecoration(
          color: Colors.green[600],
        ),
        child: Icon(Icons.settings));
    return Scaffold(
      bottomNavigationBar: Row(
        children: <Widget>[container, container2, container3],
      ),
      appBar: AppBar(
        title: Text(
          'เครื่องดื่ม',
          textAlign: TextAlign.center,
        ),
        backgroundColor: Colors.green,
      ),
      body: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('ชาเย็น 1 แก้ว ให้พลังงาน 100 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('น้ำมะนาว 1 แก้ว ให้พลังงาน 100 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('น้ำมะพร้าว 1 แก้ว ให้พลังงาน 120 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('โกโก้ 1 แก้ว ให้พลังงาน 210 Kcal'),
              ])),
          Container(
              margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(children: [
                Text('น้ำลำไย 1 แก้ว ให้พลังงาน 100 Kcal'),
              ])),
        ],
      ),
    );
  }
}
